class Board
  attr_accessor :grid

  def initialize(grid = Array.new(3) { Array.new(3) })
    @grid = grid
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos, mark)
    if !self[pos].nil?
      raise "Position occupied"
    else
      self[pos] = mark
    end
  end

  def empty?(pos)
    self[pos].nil?
  end

  def winner
    (rows + columns + diagonals).each do |line|
      return :X if line == [:X, :X, :X]
      return :O if line == [:O, :O, :O]
    end

    nil
  end

  def diagonals
    diags = [[], []]
    (0..2).each do |i|
      diags[0] << self[[i, i]]
      diags[1] << self[[i, 2 - i]]
    end

    diags
  end

  def rows
    rows = []
    @grid.each { |row| rows << row }
    rows
  end

  def columns
    columns = [[], [], []]
    (0..2).each do |row|
      (0..2).each do |col|
        columns[col] << self[[row, col]]
      end
    end

    columns
  end

  def tie?
    nil_count = 0
    (0..2).each do |row|
      (0..2).each do |col|
        nil_count += 1 if self[[row, col]].nil?
      end
    end

    return true if nil_count == 0
    false
  end

  def over?
    return true if winner || tie?
    false
  end
end

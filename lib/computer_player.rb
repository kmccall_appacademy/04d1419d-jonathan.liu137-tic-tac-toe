class ComputerPlayer
  attr_accessor :name, :mark, :board

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    win_move = winning_move
    return win_move if !winning_move.nil?
    valid_moves.shuffle.first
  end

  def valid_moves
    valids = []
    (0..2).each do |row|
      (0..2).each do |col|
        valids << [row, col] if board[[row, col]].nil?
      end
    end

    valids
  end

  def winning_move
    valid_moves.each do |pos|
      board[pos] = mark

      if board.winner == mark
        board[pos] = nil
        return pos
      end

      board[pos] = nil
    end

    nil
  end



end

require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board

  def initialize(player1, player2)
    @board = Board.new
    @p1 = player1
    @p1.mark = :X
    @p2 = player2
    @p2.mark = :O
    @current = @p1
  end

  def play_turn
    @current.display(board)
    @board.place_mark(@current.get_move, @current.mark)
    switch_players!
  end

  def current_player
    @current
  end

  def switch_players!
    if @current == @p2
      @current = @p1
    else
      @current = @p2
    end
  end

  def play
    until @board.over?
      play_turn
    end

    if board.winner == :X
      puts "#{@p1.name} is the winner!"
    elsif board.winner == :O
      puts "#{@p2.name} is the winner!"
    else
      "It's a tie--cat's game!"
    end
  end

  if __FILE__ == $PROGRAM_NAME
    puts "What is your name?"
    name = gets.chomp

    game = Game.new(HumanPlayer.new(name), ComputerPlayer.new("AI"))
    game.play
  end

end

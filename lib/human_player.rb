class HumanPlayer
  attr_accessor :name, :mark, :board

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
    rows = ["   |   |   ", "   |   |   ", "   |   |   "]
    separator = "-----------"

    (0..2).each do |row|
      (0..2).each do |col|
        if !board[[row, col]].nil?
          rows[row][col * 4 + 1] = board[[row, col]].to_s
        end
      end
    end

    puts rows[0]
    puts separator
    puts rows[1]
    puts separator
    puts rows[2]
    puts ""
  end

  def get_move
    puts "Where will you move?"
    print "Please enter your next move in the form '0, 0': "
    gets.chomp.split(',').map { |num| num.to_i }
  end
end
